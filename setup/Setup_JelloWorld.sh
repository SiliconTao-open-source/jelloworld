#!/bin/ash

set -e
echo "This is JelloWorld setup"

apk add openjdk11

mkdir /opt/jelloWorld/
curl https://dl.cloudsmith.io/public/silicontao/jelloworld/maven/com/example/demo/0.0.1-SNAPSHOT/demo-0.0.1-20230123.053022-1.jar \
> /opt/jelloWorld/jelloWorld.jar

echo "Make a startup script that runs the JAR!"
